#include "player.h"


int main(int argc, char *argv[])
{
    Game game(15.0, 50, "Petr");
    game.changeHp(3.0);
    game.changeSpeed(7);

    for(size_t i = 0u; i < 2u; ++i)
    {
        game.undo();
    }

    return 0;
}
