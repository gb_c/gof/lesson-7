#ifndef PLAYER_H
#define PLAYER_H

#include <iostream>
#include <fstream>
#include <vector>
#include "player.pb.h"


class Memento
{
private:
    float _hp;
    int _speed;
    std::string _name;

public:
    Memento() : _hp(0), _speed(0), _name("") {}
    Memento(float hp, int speed, std::string name) : _hp(hp), _speed(speed), _name(name) {}
    Memento(const Memento &other)
        : _hp(other._hp)
        , _speed(other._speed)
        , _name(other._name)
    {}

    float getHp() const
    {
        return _hp;
    }

    int getSpeed() const
    {
        return _speed;
    }

    std::string getName() const
    {
        return _name;
    }
};


class Player
{
private:
    float hp;
    int speed;
    std::string name;

public:
    Player(float hp, int speed, const std::string &name)
        : hp(hp)
        , speed(speed)
        , name(name)
    {}

    void changeSpeed(int change)
    {
        speed += change;
    }

    void changeHp(float change)
    {
        hp += change;
    }

    float getHp() const
    {
        return hp;
    }

    int getSpeed() const
    {
        return speed;
    }

    const std::string &getName() const
    {
        return name;
    }

    Memento createMemento()
    {
        Memento m(hp, speed, name);
        return m;
    }

    void reinstateMemento(const Memento &m)
    {
        hp = m.getHp();
        speed = m.getSpeed();
        name = m.getName();
    }
};


class Game
{
private:
    memento::PlayerMemento memory;
    Player p;
    std::string file = "player.bin";

    void print()
    {
        std::cout << "HP: " << p.getHp() << ". Speed: " << p.getSpeed() << ". Name: " << p.getName()
                  << ". Backups: " << memory.player_size() << "." << std::endl;
    }

    int push_back(const Memento &m)
    {
        memory.add_player();
        memory.mutable_player(memory.player_size() - 1)->set_hp(m.getHp());
        memory.mutable_player(memory.player_size() - 1)->set_speed(m.getSpeed());
        memory.mutable_player(memory.player_size() - 1)->set_name(m.getName());

        std::ofstream out(file, std::ios::binary);

        if(!memory.SerializeToOstream(&out))
        {
            std::cerr << "Failed to write memento." << std::endl;
            return -1;
        }

        return 0;
    }

    Memento pop_back()
    {
        const auto mp = memory.player(memory.player_size() - 1);
        Memento m(mp.hp(), mp.speed(), mp.name());
        memory.mutable_player()->RemoveLast();

        std::ofstream out(file, std::ios::binary);

        if(!memory.SerializeToOstream(&out))
        {
            std::cerr << "Failed to write memento." << std::endl;
        }

        return m;
    }

public:
    Game(float hp, int speed, const std::string &name) : p(hp, speed, name)
    {
        std::ifstream in(file, std::ios::binary);

        if(!in)
        {
            std::cout << file << ": File not found.  Creating a new file." << std::endl;
        }
        else
        {
            if(!memory.ParseFromIstream(&in))
            {
                std::cerr << "Failed to parse player." << std::endl;
            }
        }

        print();
    }

    void changeSpeed(int change)
    {
        push_back(p.createMemento());
        p.changeSpeed(change);
        print();
    }

    void changeHp(float change)
    {
        push_back(p.createMemento());
        p.changeHp(change);
        print();
    }

    void undo()
    {
        if(memory.player_size() > 0)
        {
            p.reinstateMemento(pop_back());
            print();
        }
        else
        {
            std::cerr << "Nothing to backup" << std::endl;
        }
    }
};

#endif // PLAYER_H
