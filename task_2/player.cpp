#include "player.h"


class Player::Impl
{
private:
    float _hp;
    int _speed;
    std::string _name;

public:
    Impl(float hp, int speed, const std::string &name)
        : _hp(hp)
        , _speed(speed)
        , _name(name)
    {}

    ~Impl() {}

    void welcomeMess()
    {
        std::cout << "Welcome, " << _name << "! Your Hp: " << _hp << ", Speed: " << _speed << std::endl;
    }

    void changeSpeed(int change)
    {
        _speed += change;
    }

    void changeHp(float change)
    {
        _hp += change;
    }

    float getHp() const
    {
        return _hp;
    }

    int getSpeed() const
    {
        return _speed;
    }

    const std::string& getName() const
    {
        return _name;
    }
};


Player::Player(float hp, int speed, const std::string &name) : pImpl(new Impl(hp, speed, name))
{
    pImpl->welcomeMess();
}

Player::Player(const Player& other) : pImpl(new Impl(*other.pImpl))
{
    pImpl->welcomeMess();
}

Player& Player::operator=(Player other)
{
    std::swap(pImpl, other.pImpl);
    return *this;
}

Player::~Player() = default;

void Player::changeSpeed(int change)
{
    int old = pImpl->getSpeed();
    pImpl->changeSpeed(change);
    std::cout << "Change Speed: " << old << " + " << change << " = " << pImpl->getSpeed() << std::endl;
}

void Player::changeHp(float change)
{
    float old = pImpl->getHp();
    pImpl->changeHp(change);
    std::cout << "Change Hp: " << old << " + " << change << " = " << pImpl->getHp() << std::endl;
}

float Player::getHp() const
{
    return pImpl->getHp();
}

int Player::getSpeed() const
{
    return pImpl->getSpeed();
}

const std::string& Player::getName() const
{
    return pImpl->getName();
}
