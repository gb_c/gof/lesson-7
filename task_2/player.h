#ifndef PLAYER_H
#define PLAYER_H

#include <iostream>
#include <memory>
#include <algorithm>


class Player
{
private:
    class Impl;
    std::unique_ptr<Impl> pImpl;

public:
    Player(float hp, int speed, const std::string &name);
    Player(const Player &other);
    Player& operator=(Player other);

    ~Player();

    void changeSpeed(int change);
    void changeHp(float change);
    float getHp() const;
    int getSpeed() const;
    const std::string &getName() const;
};

#endif // PLAYER_H
